package com.bolaandroid.benews;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;
    ArrayList<Article> newsArticle;

    public CustomPagerAdapter(Context context,ArrayList<Article> newsArticle) {
        mContext = context;
        this.newsArticle = newsArticle;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        Article article = newsArticle.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.detail_page, collection, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.image);
        TextView title = (TextView) layout.findViewById(R.id.title);
        TextView description = (TextView) layout.findViewById(R.id.description);
        TextView source = (TextView) layout.findViewById(R.id.source);
        Picasso.with(mContext).load(article.getUrlToImage()).placeholder(R.color.transparentGrey).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        ProgressBar progressBar = layout.findViewById(R.id.progress_bar);
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {}
                });
        title.setText(article.getTitle());
        description.setText(article.getDescription());
        source.setText(mContext.getString(R.string.source,article.getSource().getName(),article.getAuthor()));
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return newsArticle.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
/*


    @Override
    public CharSequence getPageTitle(int position) {
        ModelObject customPagerEnum = ModelObject.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

     */

}
