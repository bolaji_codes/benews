package com.bolaandroid.benews;

import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bolaandroid.benews.remote.ApiUtils;
import com.bolaandroid.benews.remote.NewsAPIService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator;

public class NewsActivity extends AppCompatActivity {
    public static final String DEFAULT_INTEREST="General";
    public static final String NEWS_API_KEY="8d1024a54b9b473e930770f97189febe";
    ViewPager pager;
    ProgressBar progressBar;
    TextView pageCount;
    ArrayList<Article> articles = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        pager = findViewById(R.id.view_pager);
        progressBar =  findViewById(R.id.progress_bar);
        pageCount = findViewById(R.id.page_count);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.category_array,
                        R.layout.spinner_layout);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getPosition(DEFAULT_INTEREST));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getNews(adapterView.getItemAtPosition(i).toString());
                pageCount.setText("Page 1");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void getNews(final String interest){
        showLoading();
        NewsAPIService newsAPIService = ApiUtils.getNewsAPIService();
        Call<NewsResponse> call = newsAPIService.getNews("NG",interest,NEWS_API_KEY);
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                hideLoading();
                if(response.isSuccessful()){
                    hideLoading();
                    articles=response.body().getArticles();
                    pager.setAdapter(new CustomPagerAdapter(NewsActivity.this,articles));
                    pager.addOnPageChangeListener(getPageListener());
                    ScrollingPagerIndicator indicator = findViewById(R.id.indicator);
                    indicator.attachToPager(pager);
                }else{
                    Snackbar snackbar = Snackbar.make(pager,"Error fetching from server 1", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getNews(interest);
                        }
                    });
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                hideLoading();
                Snackbar snackbar = Snackbar.make(pager,"Error fetching from server 2", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getNews(interest);
                    }
                });
                snackbar.show();
            }
        });

    }


    public void showLoading(){
        pager.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }
    public void hideLoading(){
        pager.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private ViewPager.OnPageChangeListener getPageListener(){
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                pageCount.setText(getString(R.string.page_num,position+1,articles.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }
}
