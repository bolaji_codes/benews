package com.bolaandroid.benews.remote;


public class ApiUtils {

    public static final String BASE_URL = "https://newsapi.org/";

    public static NewsAPIService getNewsAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(NewsAPIService.class);
    }
}