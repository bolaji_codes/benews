package com.bolaandroid.benews.remote;


import com.bolaandroid.benews.NewsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsAPIService {
   @GET("/v2/top-headlines/")
    Call<NewsResponse> getNews(
            @Query("country") String c1,
            @Query("category") String c2,
            @Query("apiKey") String c3
    );

}